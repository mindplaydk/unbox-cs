﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace Unbox
{
    struct Key
    {
        public readonly Type Type;

        public readonly string Name;

        public Key(Type type, string name)
        {
            Type = type;
            Name = name;
        }
    }

    class FactoryMap : Dictionary<Key, Factory>
    {
        public FactoryMap() : base() { }
        public FactoryMap(FactoryMap other) : base(other) { }
    }

    delegate object Factory(IContainer container);

    class InitializationMap : Dictionary<Key, List<Initializer>>
    {
        public InitializationMap() : base() { }
        public InitializationMap(InitializationMap other) : base(other) { }
    }

    delegate object? Initializer(IContainer container, object value);

    class InstanceMap : Dictionary<Key, object>
    {
        public InstanceMap() : base() { }
    }

    class ParameterMap : Dictionary<string, string>
    {
        public ParameterMap(Object map) : base()
        {
            foreach (var property in map.GetType().GetProperties())
            {
                Add(property.Name, (string) property.GetValue(map));
            }
        }

        public static ParameterMap From(Object? map) => map == null
            ? ParameterMap.Empty
            : new ParameterMap(map);

        public static ParameterMap Empty = new ParameterMap(new { });
    }

    public class Builder
    {
        internal FactoryMap factories = new FactoryMap();

        internal InitializationMap initializations = new InitializationMap();

        public void Register<T>(Type type, string name, Func<IContainer, T> create)
            where T : notnull
        {
            // TODO this doesn't work, but wrapping in another delegate does. wat?
            //factories[new Key(type, name)] = c;
            factories[new Key(type, name)] = c => create(c);
        }

        public void Configure(Type type, string name, Delegate init, object? map = null)
        {
            var initialization = Reflection.CreateInitializer(init, ParameterMap.From(map));

            var key = new Key(type, name);

            if (initializations.TryGetValue(key, out var list))
            {
                list.Add(initialization);
            }
            else
            {
                initializations.Add(key, new List<Initializer> { initialization });
            }
        }
    }

    public static class BuilderExtensions
    {
        public static void Register<T>(this Builder builder, string name, Func<IContainer, T> create)
            where T : class
        {
            builder.Register(typeof(T), name, create);
        }

        public static void Register<T>(this Builder builder, Func<IContainer, T> create)
            where T : class
        {
            builder.Register(typeof(T).GetDefaultComponentName(), create);
        }

        public static void Register<T>(this Builder builder, string name, Object? map = null)
            where T : class
        {
            builder.Register<T>(
                name,
                Reflection.CreateFactory<T>(ParameterMap.From(map))
            );
        }

        public static void Register<T>(this Builder builder, Object? map = null)
            where T : class
        {
            builder.Register<T>(typeof(T).GetDefaultComponentName(), map);
        }

        public static void Set<T>(this Builder builder, string name, T value)
            where T : notnull
        {
            builder.Register(typeof(T), name, _ => value);
        }

        // TODO bulk-generate Configure() methods for all Action<> and Func<> types

        public static void Configure<T>(this Builder builder, string name, Func<T, T> init, object? map = null)
        {
            builder.Configure(typeof(T), name, init, map);
        }

        public static void Configure<T>(this Builder builder, string name, Action<T> init, object? map = null)
        {
            builder.Configure(typeof(T), name, init, map);
        }

        public static void Configure<T>(this Builder builder, Func<T, T> init, object? map = null)
        {
            builder.Configure(typeof(T), typeof(T).GetDefaultComponentName(), init, map);
        }

        public static void Configure<T>(this Builder builder, Action<T> init, object? map = null)
        {
            builder.Configure(typeof(T), typeof(T).GetDefaultComponentName(), init, map);
        }

        public static Container Build(this Builder builder)
        {
            return new Container(builder.factories, builder.initializations);
        }
    }

    public interface IContainer
    {
        public object Get(Type type, string name);

        public bool Has(Type type, string name);
    }

    public class Container : IContainer
    {
        private FactoryMap factories;

        private InitializationMap initializations;

        private InstanceMap instances = new InstanceMap();

        internal Container(FactoryMap factories, InitializationMap initializations)
        {
            this.factories = new FactoryMap(factories);
            this.initializations = new InitializationMap(initializations);
        }

        public bool Has(Type type, string name)
        {
            return factories.ContainsKey(new Key(type, name));
        }

        public object Get(Type type, string name)
        {
            var key = new Key(type, name);

            if (!instances.ContainsKey(key))
            {
                instances.Add(key, CreateService(type, name));
            }

            return instances[key];
        }

        private object CreateService(Type type, string name)
        {
            var key = new Key(type, name);

            if (!factories.ContainsKey(key))
            {
                throw new ServiceNotFoundException(type, name);
            }

            var create = factories[key];

            var service = create(this);

            if (initializations.TryGetValue(key, out var initializers))
            {
                foreach (var initializer in initializers)
                {
                    var replacement = initializer(this, service);

                    if (replacement != null)
                    {
                        service = replacement;
                    }
                }
            }

            return service;
        }
    }

    public static class ContainerExtensions
    {
        public static T Get<T>(this IContainer container, string? name = null)
            where T : notnull // TODO use notnull in other methods as well?
        {
            return (T) container.Get(typeof(T), name ?? typeof(T).GetDefaultComponentName());
        }

        public static bool Has<T>(this IContainer container, string name)
            where T : class
        {
            return container.Has(typeof(T), name);
        }

        public static bool Has<T>(this IContainer container)
            where T : class
        {
            return container.Has<T>(typeof(T).GetDefaultComponentName());
        }

        public static object Invoke(this IContainer container, Delegate f, object? map = null)
        {
            return f.CreateResolver(ParameterMap.From(map))(container);
        }

        /*

        // NOTE: use this program to auto-generate the silly boilerplate methods below.

        using System;
        using System.Linq; 
        using System.Collections.Generic;
					
        public class Program
        {
	        public static void Main()
	        {
		        foreach (int n in Enumerable.Range(0,16)) {
			        var range = n > 0 ? Enumerable.Range(1,n) : new int[] {};
			        var args = String.Join(", ", range.Select(n => $"T{n}").Append("TResult"));
			
			        Console.WriteLine($"[ExcludeFromCodeCoverage] public static TResult Invoke<{args}>(this IContainer container, Func<{args}> f, object? map = null) => (TResult) container.Invoke((Delegate) f, map);");
		        }
		
		        foreach (int n in Enumerable.Range(0,16)) {
			        var range = n > 0 ? Enumerable.Range(1,n) : new int[] {};
			        var args = String.Join(", ", range.Select(n => $"T{n}"));
			        args = args.Length > 0 ? $"<{args}>" : "";
			
			        Console.WriteLine($"[ExcludeFromCodeCoverage] public static void Invoke{args}(this IContainer container, Action{args} f, object? map = null) => container.Invoke((Delegate) f, map);");
		        }
	        }
        }

        */

        [ExcludeFromCodeCoverage] public static TResult Invoke<TResult>(this IContainer container, Func<TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, TResult>(this IContainer container, Func<T1, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, TResult>(this IContainer container, Func<T1, T2, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, TResult>(this IContainer container, Func<T1, T2, T3, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, TResult>(this IContainer container, Func<T1, T2, T3, T4, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static TResult Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult>(this IContainer container, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TResult> f, object? map = null) => (TResult)container.Invoke((Delegate)f, map);

        [ExcludeFromCodeCoverage] public static void Invoke(this IContainer container, Action f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1>(this IContainer container, Action<T1> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2>(this IContainer container, Action<T1, T2> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3>(this IContainer container, Action<T1, T2, T3> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4>(this IContainer container, Action<T1, T2, T3, T4> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5>(this IContainer container, Action<T1, T2, T3, T4, T5> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6>(this IContainer container, Action<T1, T2, T3, T4, T5, T6> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> f, object? map = null) => container.Invoke((Delegate)f, map);
        [ExcludeFromCodeCoverage] public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(this IContainer container, Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> f, object? map = null) => container.Invoke((Delegate)f, map);

        internal static object[] Resolve(this IContainer container, ParameterInfo[] parameters, ParameterMap map)
        {
            return parameters
                .Select(p => {
                    var type = p.ParameterType;
                    var name = p.ParameterType.GetDefaultComponentName();

                    if (map.TryGetValue(p.Name, out var componentName))
                    {
                        if (container.Has(type, componentName))
                        {
                            return container.Get(type, componentName);
                        }
                    }
                    else
                    {
                        if (container.Has(type, name))
                        {
                            return container.Get(type, name);
                        }

                        if (p.HasDefaultValue)
                        {
                            return p.DefaultValue;
                        }
                    }

                    // TODO add support for nullables, e.g. using Nullable.GetUnderlyingType()

                    throw new UnresolvedParameterException(p);
                })
                .ToArray();
        }
    }

    static class TypeExtensions
    {
        public static string GetDefaultComponentName(this Type type)
        {
            return type.FullName;
        }
    }

    internal static class Reflection
    {
        internal static Func<IContainer, T> CreateFactory<T>(ParameterMap map)
            where T : class
        {
            var type = typeof(T);

            var ctor = type
                .GetConstructors(BindingFlags.Public | BindingFlags.Instance)
                .OrderByDescending(c => c.GetParameters().Length)
                .FirstOrDefault();

            // TODO QA: what if there are multiple constructors with the same number,
            //          but different types of parameters? we will end up invoking a
            //          random one. Funq also appears to have this issue.

            if (ctor == null)
            {
                throw new ConstructorNotFoundException(type);
            }

            var parameters = ctor.GetParameters();

            ValidateParameters(parameters, map);

            return container => (T) ctor.Invoke(container.Resolve(parameters, map));
        }

        internal static Factory CreateResolver(this Delegate f, ParameterMap map)
        {
            var parameters = f.GetMethodInfo().GetParameters();

            ValidateParameters(parameters, map);

            return container => f.DynamicInvoke(container.Resolve(parameters, map));
        }

        internal static Initializer CreateInitializer(this Delegate f, ParameterMap map)
        {
            var parameters = f.GetMethodInfo().GetParameters().Skip(1).ToArray();

            ValidateParameters(parameters, map);

            return (container, value) => f.DynamicInvoke(container.Resolve(parameters, map).Prepend(value).ToArray());
        }

        internal static void ValidateParameters(ParameterInfo[] parameters, ParameterMap map)
        {
            foreach (var parameterName in map.Keys)
            {
                if (!parameters.Any(parameter => parameter.Name == parameterName))
                {
                    throw new UnmatchedParameterException(parameterName);
                }
            }
        }
    }

    public class ServiceNotFoundException : Exception
    {
        public ServiceNotFoundException(Type type, string name) : base($"Component not found: \"{name}\" [{type.FullName}]") { }
    }

    public class ConstructorNotFoundException : Exception
    {
        public ConstructorNotFoundException(Type type) : base($"No public constructor found for type: {type.FullName}") { }
    }

    public class UnresolvedParameterException : Exception
    {
        public UnresolvedParameterException(ParameterInfo param) : base($"Unresolved parameter \"{param.Name}\" [{param.ParameterType.FullName}]") { }
    }

    public class UnmatchedParameterException : Exception
    {
        public UnmatchedParameterException(string name) : base($"Unmatched parameter name: \"{name}\"") { }
    }
}
