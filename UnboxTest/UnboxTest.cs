using Xunit;
using Unbox;
using System.Collections.Generic;
using System.Linq;

namespace UnboxTest
{
    public class UnboxTest
    {
        [Fact]
        public void Core_Builder_and_Container_behavior()
        {
            var builder = new Builder();

            const string NAME_A = "A";
            const string NAME_B = "B";
            const string UNREGISTERED_NAME = "some.unregistered.name";
            const string NAME_PATH = "cache.path";

            const string PATH = "/temp";

            ICache cacheB = new Cache(PATH);

            builder.Register<ICache>(c => new Cache(c.Get<string>(NAME_PATH)));
            builder.Register<ICache>(NAME_A, c => new Cache(PATH));
            builder.Register(c => new Repository(c.Get<ICache>()));
            builder.Set(NAME_B, cacheB);
            builder.Set(NAME_PATH, PATH);

            var container = builder.Build();

            var cache = container.Get<ICache>();

            Assert.True(
                container.Has<ICache>(),
                "can test for registered component by default name"
            );

            Assert.True(
                container.Has<ICache>(NAME_A),
                "can test for registered component by name"
            );

            Assert.False(
                container.Has<ICache>(UNREGISTERED_NAME),
                "can test for non-registered component by name"
            );

            Assert.False(
                container.Has<UnregisteredService>(),
                "can test for non-registered component by default name"
            );

            Assert.True(
                cache is ICache,
                "can create object component"
            );

            Assert.True(
                container.Get<string>(NAME_PATH) == PATH,
                "can create value component"
            );

            Assert.True(
                cache == container.Get<ICache>(),
                "can get the same component"
            );

            Assert.True(
                cache != container.Get<ICache>(NAME_A),
                "can get same type of component with different name"
            );

            Assert.True(
                cacheB == container.Get<ICache>(NAME_B),
                "can get injected component instance"
            );

            Assert.True(
                container.Get<Repository>() is Repository,
                "can register and resolve with default component name"
            );

            Assert.Equal(
                "Component not found: \"UnboxTest.UnregisteredService\" [UnboxTest.UnregisteredService]",
                Assert.Throws<ServiceNotFoundException>(() => container.Get<UnregisteredService>()).Message
            );

            Assert.Equal(
                "Component not found: \"some.unregistered.name\" [UnboxTest.Repository]",
                Assert.Throws<ServiceNotFoundException>(() => container.Get<Repository>(UNREGISTERED_NAME)).Message
            );
        }

        [Fact]
        public void Can_resolve_constructor_dependencies()
        {
            var builder = new Builder();

            builder.Register<ServiceWithMultipleConstructors>();
            builder.Register<ServiceA>();
            builder.Register<ServiceB>();
            builder.Register<ServiceC>();
            builder.Register<ServiceWithUnregisteredDeps>();
            builder.Register<ServiceWithUnregisteredDefaultDeps>();

            var container = builder.Build();

            Assert.True(
                container.Get<ServiceWithMultipleConstructors>() is ServiceWithMultipleConstructors,
                "can resolve constructor dependencies"
            );

            Assert.True(
                container.Get<ServiceWithMultipleConstructors>().A == container.Get<ServiceA>()
                &&
                container.Get<ServiceWithMultipleConstructors>().B == container.Get<ServiceB>()
                &&
                container.Get<ServiceWithMultipleConstructors>().C == container.Get<ServiceC>(),
                "invokes the constructor with the highest number of parameters"
            );

            Assert.True(
                container.Get<ServiceWithUnregisteredDefaultDeps>().Message == "Hello",
                "can resolve using default value of constructor parameter"
            );

            Assert.Equal(
                "No public constructor found for type: UnboxTest.ServiceWithNoPublicConstructor",
                Assert.Throws<ConstructorNotFoundException>(() => builder.Register<ServiceWithNoPublicConstructor>()).Message
            );

            Assert.Equal(
                "Unresolved parameter \"unregistered\" [UnboxTest.UnregisteredService]",
                Assert.Throws<UnresolvedParameterException>(() => container.Get<ServiceWithUnregisteredDeps>()).Message
            );
        }

        [Fact]
        void Can_resolve_to_named_dependencies()
        {
            var builder = new Builder();

            builder.Register<ServiceA>("A");
            builder.Register<ServiceB>("B");
            builder.Register<ServiceC>("C");

            builder.Register<ServiceWithMultipleConstructors>(new { a = "A", b = "B", c = "C" });

            var container = builder.Build();

            Assert.True(
                container.Get<ServiceWithMultipleConstructors>().A == container.Get<ServiceA>("A")
                &&
                container.Get<ServiceWithMultipleConstructors>().B == container.Get<ServiceB>("B")
                &&
                container.Get<ServiceWithMultipleConstructors>().C == container.Get<ServiceC>("C"),
                "invokes the constructor using named component instances"
            );

            Assert.Equal(
                "Unmatched parameter name: \"wrongName\"",
                Assert.Throws<UnmatchedParameterException>(() => {
                    builder.Register<ServiceWithMultipleConstructors>(new { wrongName = "WRONG_NAME" });
                }).Message
            );

            builder.Register<ServiceWithMultipleConstructors>(new { a = "A", b = "WRONG_NAME", c = "C" });

            container = builder.Build();

            Assert.Equal(
                "Unresolved parameter \"b\" [UnboxTest.ServiceB]",
                Assert.Throws<UnresolvedParameterException>(() => {
                    container.Get<ServiceWithMultipleConstructors>();
                }).Message
            );
        }

        [Fact]
        void Can_invoke_delegates()
        {
            var builder = new Builder();

            builder.Set("a", 1);
            builder.Set("b", 2);

            var container = builder.Build();

            Assert.Equal(3, container.Invoke((int a, int b) => a + b, new { a = "a", b = "b" }));

            int value = 0;

            container.Invoke((int a) => { value = a; }, new { a = "a" });

            Assert.Equal(1, value);
        }

        [Fact]
        void Can_configure_services()
        {
            var builder = new Builder();

            builder.Register<ConfigurableService>();

            builder.Configure<ConfigurableService>(service => {
                service.AddMessage("FOO");
            });

            builder.Configure<ConfigurableService>(service => {
                service.AddMessage("BAR");
            });

            var container = builder.Build();

            Assert.Equal(
                container.Get<ConfigurableService>().Messages.ToArray(),
                new string[] { "FOO", "BAR" }
            );
        }

        [Fact]
        void Can_replace_services()
        {
            var builder = new Builder();

            builder.Register<ICache>(c => new Cache("/cache"));

            builder.Configure<ICache>(cache => new CacheDecorator(cache));

            var container = builder.Build();

            var cache = container.Get<ICache>();

            Assert.True(
                container.Get<ICache>() is CacheDecorator,
                "can configure replacement service"
            );
        }
    }

    interface ICache { }

    class Cache : ICache
    {
        public string Path { get; }

        public Cache(string path)
        {
            Path = path;
        }
    }

    class CacheDecorator : ICache
    {
        public ICache Cache { get; }

        public CacheDecorator(ICache cache)
        {
            Cache = cache;
        }
    }

    class Repository
    {
        public ICache Cache { get; }

        public Repository(ICache cache)
        {
            Cache = cache;
        }
    }

    class UnregisteredService { }

    class ServiceWithNoPublicConstructor
    {
        private ServiceWithNoPublicConstructor() { }

        static ServiceWithNoPublicConstructor() { }
    }

    class ServiceA { }
    class ServiceB { }
    class ServiceC { }

    class ServiceWithMultipleConstructors
    {
        public ServiceA? A { get; }
        public ServiceB? B { get; }
        public ServiceC? C { get; }

        public ServiceWithMultipleConstructors(ServiceA a)
        {
            A = a;
        }

        public ServiceWithMultipleConstructors(ServiceA a, ServiceB b, ServiceC c)
        {
            A = a;
            B = b;
            C = c;
        }

        public ServiceWithMultipleConstructors(ServiceA a, ServiceB b)
        {
            A = a;
            B = b;
        }
    }

    class ServiceWithUnregisteredDeps
    {
        public ServiceWithUnregisteredDeps(UnregisteredService unregistered) { }
    }

    class ServiceWithUnregisteredDefaultDeps
    {
        public string Message { get; }

        public ServiceWithUnregisteredDefaultDeps(string message = "Hello")
        {
            Message = message;
        }
    }

    class ConfigurableService
    {
        private readonly List<string> messages = new List<string> { };

        public IEnumerable<string> Messages {
            get {
                return messages;
            }
        }

        public void AddMessage(string message)
        {
            messages.Add(message);
        }
    }
}
